package lobos;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import dao.MessageDao;
import dao.PartidaDao;
import dao.RolDao;
import dao.RolJugadorPartidaDao;
import dao.UserDao;
import dao.VotDao;
import dao.xatMessageDao;
import lobos.model.Message;
import lobos.model.Partida;
import lobos.model.Rol;
import lobos.model.User;
import lobos.model.xatMessage;

public class Main2 {
	public static void main(String[] args) {
		User u1 = new User("aaaaa", "aaaaa", "aja", "c:\\");
		UserDao uDAO = new UserDao();
		uDAO.saveOrUpdate(u1);

		User u2 = new User("bbbbb", "bbbbb", "ojo", "c:\\");
		User u3 = new User("ccccc", "ccccc", "eje", "c:\\");
		User u4 = new User("ddddd", "ddddd", "uju", "c:\\");
		uDAO.saveOrUpdate(u2);
		uDAO.registre("PeterFray", "pet", "Peter", "c:\\");
		uDAO.login("PeterFray", "pet");

		Message m1 = new Message("aRgo", "noporn", u1, u2);
		MessageDao messageDAO = new MessageDao();
		messageDAO.saveOrUpdate(m1);
		//messageDAO.enviaMissatge("PeterFray", "bbbbb", "tremendoType", "Ganamos easy peasy lemon squeezy");
		messageDAO.enviaMissatge("aaaaa", "bbbbb", "tremendoType", "Te humillo");
		//messageDAO.enviaMissatge("PeterFray", "aaaaa", "tremendoType", "Te mato");
		//messageDAO.enviaMissatge("aaaaa", "PeterFray", "tremendoType", "Pal subsu");
		// messageDAO.getMissatges(u1);

		Rol r1 = new Rol("Humano", 0, "c:\\", "DESCRIPCION");
		Rol r2 = new Rol("Lobo", 4, "c:\\", "DESCRIPCION");
		Rol r3 = new Rol("Vidente", -1, "c:\\", "DESCRIPCION");
		RolDao rDAO = new RolDao();
		rDAO.saveOrUpdate(r1);
		rDAO.saveOrUpdate(r2);
		rDAO.saveOrUpdate(r3);
		Set<User> userPartida = new HashSet<User>();
		userPartida.add(u1);
		userPartida.add(u2);

		/*
		 * Set<Vot> votos = new HashSet<Vot>(); Set<RolJugadorPartida> roles = new
		 * HashSet<RolJugadorPartida>(); Set<Mort> muertos = new HashSet<Mort>();
		 */
		PartidaDao pDAO = new PartidaDao();
		Partida p = new Partida(userPartida, 2);
		pDAO.saveOrUpdate(p);
		RolJugadorPartidaDao rjpDAO = new RolJugadorPartidaDao();
		pDAO.inici(1);
		VotDao vDAO = new VotDao();
		//vDAO.vota("aaaaa", "bbbbb", 1);
		Set<xatMessage> xat = new HashSet<xatMessage>();
		xatMessage xM = new xatMessage("tremenda partida", u1, p);
		xatMessage xM2 = new xatMessage("tremenda mierda de DAO", u2, p);
		xat.add(xM);
		xat.add(xM2);
		xatMessageDao xMDAO = new xatMessageDao();
		xMDAO.saveOrUpdate(xM);
		xMDAO.saveOrUpdate(xM2);
		xMDAO.getXat(1);
		xMDAO.getXatLlops(1, "bbbbb");
		messageDAO.getMissatges(u1.getUserName());
		userPartida.add(u1);
		userPartida.add(u2);
		Partida p2 = new Partida(userPartida, 0);
		pDAO.saveOrUpdate(p2);
		pDAO.unirse(u3.getUserName(), 1);
		pDAO.unirse(u4.getUserName(), 1);
		// pDAO.inici(p2);
		//System.out.println(pDAO.jugadorsVius(2));
		//System.out.println(pDAO.rolsVius(2));
	}

}