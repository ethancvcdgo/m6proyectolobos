package lobos;

import java.util.Date;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.service.ServiceRegistry;

import lobos.model.Partida;
import lobos.model.Rol;
import lobos.model.User;

public class Main {

	static Session session;
	static SessionFactory sessionFactory;
	static ServiceRegistry serviceRegistry;

	public static synchronized SessionFactory getSessionFactory() {
		if (sessionFactory == null) {
			serviceRegistry = new StandardServiceRegistryBuilder().configure("hibernate.cfg.xml").build();
			sessionFactory = new MetadataSources(serviceRegistry).buildMetadata().buildSessionFactory();
		}
		return sessionFactory;
	}

	public static void main(String[] args) {

		try {
			session = getSessionFactory().openSession();
			session.beginTransaction();
			// cosas
			for (int i = 1; i <= 20; i++) {
				User u = new User("User" + i, "super3", "U" + i, "ufauij.png");
				session.save(u);
			}
			for (int i = 1; i <= 3; i++) {
				//Partida u = new Partida(1);
				//session.save(u);
			}

			Rol r = new Rol("Lobo", 4, "ff", "Lobete");
			Rol r2 = new Rol("Humano", 0, "ff", "Aldeano");
			Rol r3 = new Rol("Vidente", -1, "ff", "Ve cosas");
			session.save(r);
			session.save(r2);
			session.save(r3);

			session.getTransaction().commit();
			System.out.println("Todo ha salido a pedir de Milhouse");

		} catch (HibernateException e) {
			e.printStackTrace();
			// si fallase la generacion de sesion y fuese null eso seria una excepcion que
			// lanzaria otra excepcion encadenada.
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				// haces rollback de la transaccion. los cambios no se efectuan para TODA la
				// transaccion
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			// finally, lo ejecuta tanto si hace el try correctamente como si hay excepcion
			// y salta el catch
		} finally {
			if (session != null) {
				// cierra la sesion si existe
				session.close();
			}
		}

	}
}