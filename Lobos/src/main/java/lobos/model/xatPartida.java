package lobos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "xatPartida")
public class xatPartida {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	@Column(name = "Partida")
	private int Partida;
	@Column(name = "XatMessage")
	private int XatMessage;
	public xatPartida() {
		super();
	}
	public xatPartida(int id, int partida, int xatMessage) {
		super();
		this.id = id;
		Partida = partida;
		XatMessage = xatMessage;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPartida() {
		return Partida;
	}
	public void setPartida(int partida) {
		Partida = partida;
	}
	public int getXatMessage() {
		return XatMessage;
	}
	public void setXatMessage(int xatMessage) {
		XatMessage = xatMessage;
	}
	
}
