package lobos.model;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "User")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	@Column(name = "userName", nullable = false)
	private String userName;
	@Column(name = "password")
	private String password;
	@Column(name = "alias")
	private String alias;
	@Column(name = "dataRegistre")
	private Date dataRegistre;
	@Column(name = "pathAvatar")
	private String pathAvatar;
	@Column(name = "percentatgeVict")
	private double percentatgeVict;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	Set<RolJugadorPartida> RolJugPart = new HashSet<RolJugadorPartida>();
	
	@OneToMany(mappedBy = "mesSender", cascade = CascadeType.ALL)
	Set<Message> mesSender = new HashSet<Message>();
	
	@OneToMany(mappedBy = "mesReceiver", cascade = CascadeType.ALL)
	Set<Message> mesReceiver = new HashSet<Message>();
	
	@OneToMany(mappedBy = "votSender", cascade = CascadeType.ALL)
	Set<Vot> votSender = new HashSet<Vot>();
	
	@OneToMany(mappedBy = "votReceiver", cascade = CascadeType.ALL)
	Set<Vot> votReceiver = new HashSet<Vot>();
	
	@OneToMany(mappedBy = "xatSender", cascade = CascadeType.ALL)
	Set<xatMessage> xatSender = new HashSet<xatMessage>();
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
	Set<Mort> user = new HashSet<Mort>();
	
	@ManyToMany(cascade = { CascadeType.REFRESH })
	@JoinTable(name = "UsuarisPartida", joinColumns = { @JoinColumn(name = "IdUser") }, inverseJoinColumns = {
			@JoinColumn(name = "IdPartida") })
	private Set<Partida> UsuarisPartida = new HashSet<Partida>();

	public User(String userName, String password, String alias, String pathAvatar) {
		super();
		this.userName = userName;
		this.password = password;
		this.alias = alias;
		this.dataRegistre = new Date();
		this.pathAvatar = pathAvatar;
	}

	public User() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Date getDataRegistre() {
		return dataRegistre;
	}

	public void setDataRegistre(Date dataRegistre) {
		this.dataRegistre = dataRegistre;
	}

	public String getPathAvatar() {
		return pathAvatar;
	}

	public void setPathAvatar(String pathAvatar) {
		this.pathAvatar = pathAvatar;
	}

	public double getPercentatgeVict() {
		return percentatgeVict;
	}

	public void setPercentatgeVict(int percentatgeVict) {
		this.percentatgeVict = percentatgeVict;
	}

	public Set<RolJugadorPartida> getRolJugPart() {
		return RolJugPart;
	}

	public void setRolJugPart(Set<RolJugadorPartida> rolJugPart) {
		RolJugPart = rolJugPart;
	}

	public Set<Message> getMesSender() {
		return mesSender;
	}

	public void setMesSender(Set<Message> mesSender) {
		this.mesSender = mesSender;
	}

	public Set<Message> getMesReceiver() {
		return mesReceiver;
	}

	public void setMesReceiver(Set<Message> mesReceiver) {
		this.mesReceiver = mesReceiver;
	}

	public Set<Vot> getVotSender() {
		return votSender;
	}

	public void setVotSender(Set<Vot> votSender) {
		this.votSender = votSender;
	}

	public Set<Vot> getVotReceiver() {
		return votReceiver;
	}

	public void setVotReceiver(Set<Vot> votReceiver) {
		this.votReceiver = votReceiver;
	}

	public Set<xatMessage> getXatSender() {
		return xatSender;
	}

	public void setXatSender(Set<xatMessage> xatSender) {
		this.xatSender = xatSender;
	}

	public Set<Mort> getUser() {
		return user;
	}

	public void setUser(Set<Mort> user) {
		this.user = user;
	}

	public Set<Partida> getUsuarisPartida() {
		return UsuarisPartida;
	}

	public void setUsuarisPartida(Set<Partida> usuarisPartida) {
		UsuarisPartida = usuarisPartida;
	}

	public void setPercentatgeVict(double percentatgeVict) {
		this.percentatgeVict = percentatgeVict;
	}

}
