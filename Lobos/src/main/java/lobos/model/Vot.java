package lobos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Vot")
public class Vot {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	
	@Column(name = "torn")
	private int torn;
	
	@ManyToOne
	@JoinColumn(name = "votSender")
	private User votSender;
	
	@ManyToOne
	@JoinColumn(name = "votReceiver")
	private User votReceiver;
	
	@ManyToOne
	@JoinColumn(name = "votPartida")
	private Partida votPartida;

	public Vot() {
		super();
	}
	public Vot(int torn) {
		super();
		this.torn = torn;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getTorn() {
		return torn;
	}
	public void setTorn(int torn) {
		this.torn = torn;
	}
	public User getVotSender() {
		return votSender;
	}
	public void setVotSender(User votSender) {
		this.votSender = votSender;
	}
	public User getVotReceiver() {
		return votReceiver;
	}
	public void setVotReceiver(User votReceiver) {
		this.votReceiver = votReceiver;
	}
	public Partida getVotPartida() {
		return votPartida;
	}
	public void setVotPartida(Partida votPartida) {
		this.votPartida = votPartida;
	}
	
}
