package lobos.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name = "RolJugadorPartida")
public class RolJugadorPartida {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	
	@Type(type = "numeric_boolean")
	@Column(name = "viu")
	private boolean viu;
	
	@ManyToOne
	@JoinColumn(name = "rol")
	private Rol rol;
	
	@ManyToOne
	@JoinColumn(name = "partida")
	private Partida partida;
	
	@ManyToOne
	@JoinColumn(name = "user")
	private User user;

	

	public RolJugadorPartida(boolean viu, Rol rol, Partida partida, User user) {
		super();
		this.viu = viu;
		this.rol = rol;
		this.partida = partida;
		this.user = user;
	}

	public RolJugadorPartida() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isViu() {
		return viu;
	}

	public void setViu(boolean viu) {
		this.viu = viu;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Partida getPartida() {
		return partida;
	}

	public void setPartida(Partida partida) {
		this.partida = partida;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
