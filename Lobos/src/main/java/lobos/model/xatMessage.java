package lobos.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "xatMessage")
public class xatMessage {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	@Column(name = "content")
	private String content;
	@Column(name = "date")
	private Date date;
	@ManyToOne
	@JoinColumn(name = "xatSender")
	private User xatSender;
	@ManyToOne
	@JoinColumn(name = "xatPartida")
	private Partida xatPartida;

	public xatMessage(String content, User xatSender, Partida xatPartida) {
		super();
		this.content = content;
		this.date = new Date();
		this.xatSender = xatSender;
		this.xatPartida = xatPartida;
	}
	public xatMessage() {
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public User getXatSender() {
		return xatSender;
	}
	public void setXatSender(User xatSender) {
		this.xatSender = xatSender;
	}
	public Partida getXatPartida() {
		return xatPartida;
	}
	public void setXatPartida(Partida xatPartida) {
		this.xatPartida = xatPartida;
	}
	
}
