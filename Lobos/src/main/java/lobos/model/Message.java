package lobos.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "Message")
public class Message {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;
	@Column(name = "type")
	private String type;
	@Column(name = "content")
	private String content;
	@Column(name = "date")
	private Date date;
	@ManyToOne
	@JoinColumn(name = "mesSender")
	private User mesSender;
	@ManyToOne
	@JoinColumn(name = "mesReceiver")
	private User mesReceiver;

	public Message() {

	}

	public Message(String type, String content, User mesSender, User mesReceiver) {
		super();
		this.type = type;
		this.content = content;
		this.date = new Date();
		this.mesSender = mesSender;
		this.mesReceiver = mesReceiver;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getMesSender() {
		return mesSender;
	}

	public void setMesSender(User mesSender) {
		this.mesSender = mesSender;
	}

	public User getMesReceiver() {
		return mesReceiver;
	}

	public void setMesReceiver(User mesReceiver) {
		this.mesReceiver = mesReceiver;
	}

}
