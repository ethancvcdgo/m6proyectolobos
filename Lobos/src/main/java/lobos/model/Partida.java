package lobos.model;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

//TODO el xat 
@Entity
@Table(name = "Partida")
public class Partida {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", updatable = false, nullable = false)
	private int id;

	@Column(name = "torn")
	private int torn;

	@OneToMany(mappedBy = "partida", cascade = CascadeType.ALL)
	Set<RolJugadorPartida> RolJugPart = new HashSet<RolJugadorPartida>();

	@OneToMany(mappedBy = "votPartida", cascade = CascadeType.ALL)
	Set<Vot> votPartida = new HashSet<Vot>();

	@OneToMany(mappedBy = "mortPartida", cascade = CascadeType.ALL)
	Set<Mort> mortPartida = new HashSet<Mort>();

	@OneToMany(mappedBy = "xatPartida", cascade = CascadeType.ALL)
	Set<xatMessage> xatPartida = new HashSet<xatMessage>();

	@ManyToMany(cascade = { CascadeType.ALL }, mappedBy = "UsuarisPartida")
	private Set<User> UsuarisPartida = new HashSet<User>();

	public Partida() {
		super();
	}

	public Partida(Set<User> UsuarisPartida, int torn) {
		super();
		this.UsuarisPartida = UsuarisPartida;
		this.torn = torn;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getTorn() {
		return torn;
	}

	public void setTorn(int torn) {
		this.torn = torn;
	}

	public Set<RolJugadorPartida> getRolJugPart() {
		return RolJugPart;
	}

	public void setRolJugPart(Set<RolJugadorPartida> rolJugPart) {
		RolJugPart = rolJugPart;
	}

	public Set<Vot> getVotPartida() {
		return votPartida;
	}

	public void setVotPartida(Set<Vot> votPartida) {
		this.votPartida = votPartida;
	}

	public Set<Mort> getMortPartida() {
		return mortPartida;
	}

	public void setMortPartida(Set<Mort> mortPartida) {
		this.mortPartida = mortPartida;
	}

	public Set<xatMessage> getXatPartida() {
		return xatPartida;
	}

	public void setXatPartida(Set<xatMessage> xatPartida) {
		this.xatPartida = xatPartida;
	}

	public Set<User> getUsuarisPartida() {
		return UsuarisPartida;
	}

	public void setUsuarisPartida(Set<User> usuarisPartida) {
		UsuarisPartida = usuarisPartida;
	}

}
