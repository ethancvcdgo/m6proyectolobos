package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import lobos.model.Mort;

public class MortDao extends GenericDao<Mort, Integer> implements IMortsDao {

	@Override
	public List<Mort> getMorts(int idPartida, int torn) {
		Session session = sessionFactory.getCurrentSession();
		List<Mort> morts = null;
		try {
			session.beginTransaction();
			morts = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<Mort> muertosPart = new ArrayList<Mort>();
		for (Mort mort : morts) {
			if (mort.getTorn() == torn && mort.getMortPartida().getId() == idPartida) {
				muertosPart.add(mort);
			}
		}
		return muertosPart;
	}
}
