package dao;

import java.io.Serializable;
import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;

//import conrelaciones.Profesor;

public class LoggingInterceptor extends EmptyInterceptor {

   private static final long serialVersionUID = 1L;
   // Define a static logger
   //private static Logger logger = LogManager.getLogger(LoggingInterceptor.class);

   @Override
   public boolean onSave(Object entity, Serializable id, Object[] state,
            String[] propertyNames, Type[] types) {
     System.out.println("onSave method is called.");

      return super.onSave(entity, id, state, propertyNames, types);
      
   }
   
   @Override
   public boolean onLoad(Object entity, Serializable id, Object[] state,
            String[] propertyNames, Type[] types) {
     System.out.println("onLoad method is called.");
     System.out.println(id);

      return super.onLoad(entity, id, state, propertyNames, types);
      
   }
   
   
   
   
   @Override
   public String onPrepareStatement(String sql) {
      return super.onPrepareStatement(sql);
   }
   
   

}