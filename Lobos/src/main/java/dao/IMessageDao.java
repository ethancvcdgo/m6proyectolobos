package dao;

import java.util.List;

import lobos.model.Message;

public interface IMessageDao extends IGenericDao<Message, Integer> {
	void saveOrUpdate(Message m);

	Message get(Integer id);

	List<Message> list();

	void delete(Integer id);

	// hay un m�todo nuevo!
	void enviaMissatge(String usernameSender, String usernameReceiver, String type, String content);
	
	List<Message> getMissatges(String username);
}
