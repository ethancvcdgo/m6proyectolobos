package dao;

import java.util.List;

import lobos.model.Rol;

public interface IRolDao extends IGenericDao<Rol, Integer> {
	void saveOrUpdate(Rol r);

	Rol get(Integer id);

	List<Rol> list();

	void delete(Integer id);
}
