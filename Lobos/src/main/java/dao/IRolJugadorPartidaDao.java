package dao;

import java.util.List;

import lobos.model.Rol;
import lobos.model.RolJugadorPartida;
import lobos.model.User;

public interface IRolJugadorPartidaDao extends IGenericDao<RolJugadorPartida, Integer> {
	void saveOrUpdate(RolJugadorPartida p);

	RolJugadorPartida get(Integer id);

	List<RolJugadorPartida> list();

	void delete(Integer id);

	List<User> jugadorsVius(int idPartida);

	List<Rol> rolsVius(int idPartida);
	
	List<Rol> descobrirRol(int idPartida, String username);
}
