package dao;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import lobos.model.Partida;
import lobos.model.Rol;
import lobos.model.RolJugadorPartida;
import lobos.model.User;

public class PartidaDao extends GenericDao<Partida, Integer> implements IPartidaDao {

	@Override
	public void unirse(String username, int idPartida) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<Partida> partidas = null;
		try {
			session.beginTransaction();
			partidas = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<User> users = null;
		try {
			session.beginTransaction();
			UserDao u = new UserDao();
			users = u.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		User user = null;
		for (User u : users) {
			if (u.getUserName().equals(username)) {
				user = u;
			}
		}
		for (Partida partida : partidas) {
			if (partida.getId() == idPartida) {
				try {
					session.beginTransaction();
					Set<User> p = partida.getUsuarisPartida();
					p.add(user);
					partida.setUsuarisPartida(p);
					saveOrUpdate(partida);
					session.getTransaction().commit();
				} catch (Exception e) {
					e.printStackTrace();
					if (session != null && session.getTransaction() != null) {
						System.out.println("\n.......Transaction Is Being Rolled Back.......");
						session.getTransaction().rollback();
					}
					e.printStackTrace();
				}
				break;
			}
		}
	}

	@Override
	public void inici(int idPartida) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<Partida> partidas = null;
		List<Rol> roles = null;
		try {
			session.beginTransaction();
			partidas = this.list();
			RolDao r = new RolDao();
			roles = r.list();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		Rol lobo = null;
		Rol Vidente = null;
		Rol Ald = null;
		for (Rol rol : roles) {
			if (rol.getNom().equals("Lobo")) {
				lobo = rol;
			} else if (rol.getNom().equals("Vidente")) {
				Vidente = rol;
			} else {
				Ald = rol;
			}
		}
		Partida part = null;
		for (Partida partida : partidas) {
			if (partida.getId() == idPartida) {
				part = partida;
				break;
			}
		}
		Set<RolJugadorPartida> rolJugPart = new HashSet<RolJugadorPartida>();
		Set<User> users = part.getUsuarisPartida();
		int lobos = users.size() / 4;
		int vidente = 1;
		for (User user : users) {
			if (vidente > 0) {
				RolJugadorPartida rjp = new RolJugadorPartida(true, Vidente, part, user);
				rolJugPart.add(rjp);
				vidente--;
			} else if (lobos > 0) {
				RolJugadorPartida rjp = new RolJugadorPartida(true, lobo, part, user);
				rolJugPart.add(rjp);
				lobos--;
			} else {
				RolJugadorPartida rjp = new RolJugadorPartida(true, Ald, part, user);
				rolJugPart.add(rjp);
			}
		}
		part.setRolJugPart(rolJugPart);
		part.setTorn(1);
		try {
			saveOrUpdate(part);
			session.getTransaction().commit();
		} catch (Exception e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}
	
	
}
