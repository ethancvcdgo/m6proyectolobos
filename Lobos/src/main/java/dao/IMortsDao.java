package dao;

import java.util.List;
import lobos.model.Mort;

public interface IMortsDao extends IGenericDao<Mort, Integer> {
	void saveOrUpdate(Mort m);

	Mort get(Integer id);

	List<Mort> list();

	void delete(Integer id);

	List<Mort> getMorts(int idPartida, int torn);
}
