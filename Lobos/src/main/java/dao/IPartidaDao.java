package dao;

import java.util.List;
import lobos.model.Partida;

public interface IPartidaDao extends IGenericDao<Partida, Integer> {
	void saveOrUpdate(Partida p);

	Partida get(Integer id);

	List<Partida> list();

	void delete(Integer id);

	void unirse(String username, int idPartida);

	void inici(int idPartida);
}
