package dao;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import lobos.model.User;

public class UserDao extends GenericDao<User, Integer> implements IUserDao {

	@Override
	public boolean registre(String username, String pw, String alias, String pathAvatar) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		User u = new User(username, pw, alias, pathAvatar);
		try {
			session.beginTransaction();
			session.saveOrUpdate(u);
			session.getTransaction().commit();
			return true;
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean login(String username, String pw) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<User> users;
		try {
			session.beginTransaction();
			users = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
			return false;
		}
		for (User user : users) {
			if (user.getUserName().equals(username) && user.getPassword().equals(pw)) {
				return true;
			}
		}
		return false;
	}

}
