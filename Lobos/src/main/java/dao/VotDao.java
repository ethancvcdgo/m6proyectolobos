package dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import lobos.model.Partida;
import lobos.model.RolJugadorPartida;
import lobos.model.User;
import lobos.model.Vot;

public class VotDao extends GenericDao<Vot, Integer> implements IVotDao {

	@Override
	public void vota(String usernameSender, String usernameReceiver, Integer idPartida) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<User> users = null;
		try {
			UserDao u = new UserDao();
			session.beginTransaction();
			users = u.list();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<Partida> partidas = null;
		try {
			PartidaDao p = new PartidaDao();
			partidas = p.list();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<RolJugadorPartida> RolJugadorPartidas = null;
		try {
			RolJugadorPartidaDao p = new RolJugadorPartidaDao();
			RolJugadorPartidas = p.list();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		User Sender = null;
		User Receiver = null;
		for (User user : users) {
			if (user.getUserName().equals(usernameSender)) {
				Sender = user;
			}
			if (user.getUserName().equals(usernameReceiver)) {
				Receiver = user;
			}
		}
		Partida part = null;
		for (Partida Partida : partidas) {
			if (Partida.getUsuarisPartida().contains(Sender)) {
				part = Partida;
			}
		}
		if (part.getTorn() % 2 == 0) {
			for (RolJugadorPartida rolJugadorPartida : RolJugadorPartidas) {
				if (rolJugadorPartida.getUser().getId() == Sender.getId()) {
					if (rolJugadorPartida.getRol().getNom().equals("Lobo")) {
						Vot v = new Vot(part.getTorn());
						v.setVotReceiver(Receiver);
						v.setVotSender(Sender);
						v.setVotPartida(part);
						try {
							session.saveOrUpdate(v);
							session.getTransaction().commit();
						} catch (HibernateException e) {
							e.printStackTrace();
							if (session != null && session.getTransaction() != null) {
								System.out.println("\n.......Transaction Is Being Rolled Back.......");
								session.getTransaction().rollback();
							}
							e.printStackTrace();
						}
						break;
					}
				}
			}
			
		} else {
			Vot v = new Vot(part.getTorn());
			v.setVotReceiver(Receiver);
			v.setVotSender(Sender);
			v.setVotPartida(part);
			try {
				session.saveOrUpdate(v);
				session.getTransaction().commit();
			} catch (HibernateException e) {
				e.printStackTrace();
				if (session != null && session.getTransaction() != null) {
					System.out.println("\n.......Transaction Is Being Rolled Back.......");
					session.getTransaction().rollback();
				}
				e.printStackTrace();
			}
		}

	}

	@Override
	public List<Vot> getHistorial(int idPartida, int torn) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<Vot> vots = null;
		try {
			session.beginTransaction();
			vots = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<Vot> votosPart = new ArrayList<Vot>();
		for (Vot vot : vots) {
			if (vot.getTorn() == torn && vot.getVotPartida().getId() == idPartida && torn % 2 != 0) {
				votosPart.add(vot);
			}
		}
		return votosPart;
	}

}
