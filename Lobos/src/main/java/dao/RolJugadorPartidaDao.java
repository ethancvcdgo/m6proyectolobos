package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import lobos.model.Rol;
import lobos.model.RolJugadorPartida;
import lobos.model.User;

public class RolJugadorPartidaDao extends GenericDao<RolJugadorPartida, Integer> implements IRolJugadorPartidaDao {

	@Override
	public List<User> jugadorsVius(int idPartida) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<RolJugadorPartida> partidas = null;
		try {
			session.beginTransaction();
			partidas = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<User> users = new ArrayList<User>();
		for (RolJugadorPartida rol : partidas) {
			if (rol.isViu() && rol.getPartida().getId() == idPartida) {
				users.add(rol.getUser());
			}
		}
		return users;
	}

	@Override
	public List<Rol> rolsVius(int idPartida) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<RolJugadorPartida> partidas = null;
		try {
			session.beginTransaction();
			partidas = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<Rol> rols = new ArrayList<Rol>();
		for (RolJugadorPartida rol : partidas) {
			if (rol.isViu() && rol.getPartida().getId() == idPartida) {
				rols.add(rol.getRol());
			}
		}
		return rols;
	}

	@Override
	public List<Rol> descobrirRol(int idPartida, String username) {
		Session session = sessionFactory.getCurrentSession();
		List<RolJugadorPartida> partidas = null;
		try {
			session.beginTransaction();
			partidas = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}

		List<RolJugadorPartida> RolJugadorPartidas = null; 
		try {
			RolJugadorPartidaDao p = new RolJugadorPartidaDao();
			session.beginTransaction();
			RolJugadorPartidas = p.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		RolJugadorPartida roles = null;
		for (RolJugadorPartida rolJugadorPartida : RolJugadorPartidas) {
			if (rolJugadorPartida.getPartida().getId() == idPartida
					&& rolJugadorPartida.getUser().getUserName().equals(username)) {
				roles = rolJugadorPartida;
			}
		}

		List<Rol> rols = new ArrayList<Rol>();
		if (roles.getPartida().getTorn() % 2 == 0) {
			for (RolJugadorPartida rol : partidas) {
				if (roles.isViu() && rol.isViu() && rol.getPartida().getId() == idPartida && rol.getRol().getNom().equals("Vidente")
						&& rol.getPartida().getTorn() % 2 == 0) {
					rols.add(rol.getRol());
				}
			}

		}
		
		return rols;
	}

}
