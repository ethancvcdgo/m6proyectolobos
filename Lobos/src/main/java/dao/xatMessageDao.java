package dao;

import java.util.ArrayList;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Session;

import lobos.model.Partida;
import lobos.model.RolJugadorPartida;
import lobos.model.User;
import lobos.model.xatMessage;

public class xatMessageDao extends GenericDao<xatMessage, Integer> implements IXatMessageDao {

	@Override
	public List<xatMessage> getXat(int idPartida) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<xatMessage> xats = null;
		try {
			session.beginTransaction();
			xats = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<xatMessage> xatsPartida = new ArrayList<xatMessage>();
		for (xatMessage xat : xats) {
			if (xat.getXatPartida().getId() == idPartida) {
				xatsPartida.add(xat);
			}
		}
		return xatsPartida;
	}

	@Override
	public List<xatMessage> getXatLlops(int idPartida, String username) {
		Session session = sessionFactory.getCurrentSession();
		List<xatMessage> xats = null;
		List<RolJugadorPartida> RolJugadorPartidas = null;
		try {
			session.beginTransaction();
			xats = this.list();
			RolJugadorPartidaDao p = new RolJugadorPartidaDao();
			RolJugadorPartidas = p.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		RolJugadorPartida roles = null;
		for (RolJugadorPartida rolJugadorPartida : RolJugadorPartidas) {
			if (rolJugadorPartida.getPartida().getId() == idPartida
					&& rolJugadorPartida.getUser().getUserName().equals(username)) {
				roles = rolJugadorPartida;
			}
		}

		List<xatMessage> xatsPartida = new ArrayList<xatMessage>();
		if (roles.getPartida().getTorn() % 2 == 0) {
			if (roles.getRol().getNom().equals("Lobo")) {
				for (xatMessage xat : xats) {
					if (xat.getXatPartida().getId() == idPartida) {
						xatsPartida.add(xat);
					}
				}
			}
		}
		return xatsPartida;
	}

	@Override
	public String escriureMissatgeXat(int idPartida, String missatge) {
		Session session = sessionFactory.getCurrentSession();
		List<xatMessage> xats = null;
		try {
			session.beginTransaction();
			xats = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		String missatgeNou = null;
		for (xatMessage xat : xats) {
			if (xat.getXatPartida().getId() == idPartida && xat.getXatPartida().getTorn() % 2 == 1) {
				missatgeNou = missatge;
			}
		}
		return missatgeNou;
	}

	@Override
	public String escriureMissatgeLlop(int idPartida, String username, String missatge) {
		Session session = sessionFactory.getCurrentSession();
		List<xatMessage> xats = null;
		try {
			session.beginTransaction();
			xats = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<RolJugadorPartida> RolJugadorPartidas = null;
		try {
			RolJugadorPartidaDao p = new RolJugadorPartidaDao();
			session.beginTransaction();
			RolJugadorPartidas = p.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		RolJugadorPartida roles = null;
		for (RolJugadorPartida rolJugadorPartida : RolJugadorPartidas) {
			if (rolJugadorPartida.getPartida().getId() == idPartida
					&& rolJugadorPartida.getUser().getUserName().equals(username)) {
				roles = rolJugadorPartida;
			}
		}

		String missatgeNouLlops = null;
		if (roles.getPartida().getTorn() % 2 == 0) {
			if (roles.getRol().getNom().equals("Lobo")) {
				for (xatMessage xat : xats) {
					if (xat.getXatPartida().getId() == idPartida) {
						//missatge = username+ " " +missatge;
						missatgeNouLlops = missatge;
					}
				}
			}
		}
		return missatgeNouLlops;
	}

}
