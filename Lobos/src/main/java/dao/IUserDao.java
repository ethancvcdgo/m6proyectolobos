package dao;

import java.util.List;
import lobos.model.User;

public interface IUserDao extends IGenericDao<User, Integer> {
	void saveOrUpdate(User u);

	User get(Integer id);

	List<User> list();

	void delete(Integer id);

	// hay un m�todo nuevo!
	boolean registre(String username, String pw, String alias, String pathAvatar);
	
	boolean login(String username, String pw);
}
