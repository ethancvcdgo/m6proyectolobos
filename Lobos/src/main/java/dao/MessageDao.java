package dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;

import lobos.model.Message;
import lobos.model.User;

public class MessageDao extends GenericDao<Message, Integer> implements IMessageDao {

	@Override
	public void enviaMissatge(String usernameSender, String usernameReceiver, String type, String content) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<User> users = null;
		try {
			UserDao u = new UserDao();
			session.beginTransaction();
			users = u.list();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		User Sender = null;
		User Receiver = null;
		for (User user : users) {
			if (user.getUserName().equals(usernameSender)) {
				Sender = user;
			}
			if (user.getUserName().equals(usernameReceiver)) {
				Receiver = user;
			}
		}
		Message m = new Message(type, content, Sender, Receiver);

		try {
			session.saveOrUpdate(m);
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
	}

	@Override
	public List<Message> getMissatges(String username) {
		// TODO Auto-generated method stub
		Session session = sessionFactory.getCurrentSession();
		List<Message> messages = null;
		try {
			session.beginTransaction();
			messages = this.list();
			session.getTransaction().commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			if (session != null && session.getTransaction() != null) {
				System.out.println("\n.......Transaction Is Being Rolled Back.......");
				session.getTransaction().rollback();
			}
			e.printStackTrace();
		}
		List<Message> messagesTuyos = new ArrayList<Message>();
		for (Message message : messages) {
			if (message.getMesReceiver().getUserName().equals(username)
					|| message.getMesSender().getUserName().equals(username)) {
				messagesTuyos.add(message);
			}
		}
		return messagesTuyos;
	}
}
