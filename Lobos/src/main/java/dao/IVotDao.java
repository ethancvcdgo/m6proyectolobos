package dao;

import java.util.List;
import lobos.model.Vot;

public interface IVotDao extends IGenericDao<Vot, Integer> {
	void saveOrUpdate(Vot v);

	Vot get(Integer id);

	List<Vot> list();

	void delete(Integer id);
	
	void vota(String usernameSender, String usernameReceiver, Integer idPartida);
	
	List<Vot> getHistorial(int idPartida, int torn);
}
