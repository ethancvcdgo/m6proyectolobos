package dao;

import java.util.List;
import lobos.model.xatMessage;

public interface IXatMessageDao extends IGenericDao<xatMessage, Integer> {
	void saveOrUpdate(xatMessage xM);

	xatMessage get(Integer id);

	List<xatMessage> list();

	void delete(Integer id);
	
	List<xatMessage> getXat(int idPartida);

	List<xatMessage> getXatLlops(int idPartida, String username);
	
	String escriureMissatgeXat(int idPartida, String missatge);
	
	String escriureMissatgeLlop(int idPartida, String username, String missatge);
}
